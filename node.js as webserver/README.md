#Node.js HTTP Module
##Built-in HTTP Module
Node.js memiliki built-in module yang memungkinkan node.js dapat mentrasfer data melalui HTTP.
Untuk menggunakankannya, digunakan metode ```require()```

##Node.js sebagai Web Server
HTTP Modul dapat membuat HTTP Server didengar oleh port tertentu yang akan merespon balik permintaan klien.
Fungsi yang digunakan adalah ```http.createServer()```, yang akan mengeksekusi ketika seseorang mengakses komputer pada port 8080
Seperti biasa, cara menggunakannya adalah melalui CLI > ```node webserver.js```
Lalu kunjungi ```http://localhost:8080```.

##Menambah sebuah HTTP Header
Jika respon hari HTTP server dimungkinkan ditampilkan sebagai laman HTML, maka harus ditambahkan sebuah HTTP Header dengan _content type_ yang tepat.
Sebagai contoh ```res.writeHead(200, {'Content-Type': 'text/html'});```. 
Pada Argumen pertama digunakan metode ```res.writeHead()``` sebagai status code. 200 berarti _that all is OK_. dan argumen kedua adalah objek yang berisi (containing) dari _response headers_

##Pembacaan _Query String_
Fungsi yang disematkan dalam `http.createServer()` memiliki `req` argumen yang merepresentasikan permintaan dari klien sebagai sebuah _object_ (http.IncomingMessage object).
Object yang memiliki propertu yang disebut url yang memiliki bagian dari url yang berada setelah _domain name_. Semisal jika kita mengetikkan ```localhost:8080/subdir``` maka yang akan tampil adalah `/subdir`. 
Coba jalankan file _webserver - query.js_. lalu ketikkan alamat sesuai contoh diatas. Apakah hasilnya?

##Pemisahan _Query String_
Cek pada file terakhir `webserver - query.js` untuk keterangan lebih lanjut. 
