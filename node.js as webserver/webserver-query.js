var http = require('http');
var url = require('url'); //variabel url

//membuat objek server
http.createServer(function (req, res) {
  //menambah sebuah HTTP header:
  res.writeHead(200, {'Content-Type':'text/html'});
  var q = url.parse(req.url, true).query;
  var txt = q.year + " " + q.month;
  res.end(txt);//akhir dari respon
}).listen(8080);//objek server dapat didengar melalui port 8080

//isikan pada addrees bar dengan http://localhost:8080/?year=2017&month=July
//lihat hasil yang ditampilkan
