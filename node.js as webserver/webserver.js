var http = require('http');
var uri = require('url');

//membuat objek server
http.createServer(function (req, res) {
  //menambah sebuah HTTP header:
  res.writeHead(200, {'Content-Type':'text/html'})
  res.write(req.url);//membaca query string dari URL
  res.write(' ');
  res.write('Hello World!'); //menulis respon kepada klien
  res.write(' ');
  var q = url.parse(req.url,true).query;
  var txt = q.year + " " + q.month;
  res.end();//akhir dari respon
}).listen(8080);//objek server dapat didengar melalui port 8080
