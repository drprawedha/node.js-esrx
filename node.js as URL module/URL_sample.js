var url = require('url');
var adr = 'http://localhost:8080/default.htm?year=2017&month=february';
//penguraian alamat
var q = url.parse(adr, true);

//metode pemisahan membaginya menjadi properti masing-masing
console.log(q.host);
console.log(q.pathname);
console.log(q.search);

//atau dengan query property yang mengembalikan objek dengan semua query string sebagai propertinya
var qdata = q.query;
console.log(qdata.month);
console.log(qdata.year);
console.log(qdata.host); // untuk bagian ini lebih baik menggunakan metode nomor 1 diatas
