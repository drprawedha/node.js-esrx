#Node.js File System Module
##Node.js sebagai File Server
Node.js FS Module memungkinkan kita untuk berkerja dengan _file system_ pada komputer kita. Untuk memasukkan FS Module, gunakan ```require('fs')```.
Pada umumnya penggunaan FS Module, meliputi

1. Read (Membaca)
2. Create (Membuat)
3. Update (Memperbarui)
4. Delete (Menghapus)
5. Rename (Menamai Ulang)

## _Read_ (baca) berkas
`fs.readFile()` digunakan untuk membaca berkas yang berada pada komputer. Kita asumsikan terdapat berkas bernama `berkasku.html` yang terletak di folder yang sama dengan file node.js. lalu jalankan `filesys.js` dan buka alamat `localhost:8080`. Maka berkasku.html akan terbaca sebagai konten yang di _load_ oleh node.js.

## _Create_ (buat) berkas
FS Module memiliki beberapa metode yang digunakan untuk membuat file :

- `fs.appendFile`
- `fs.open()`
- `fs.writeFile()`

Pada `fs.appendFile` digunakan untuk menambahkan konten spesifik yang jika belum ada, maka akan dibuat.

Pada `fs.open()` metode mengambil 'flag' sebagai argumen kedua. jika argumen 'w' berarti 'writing' pada berkas yang dibuka untuk ditulis (-w). jika tidak ada. maka sebuah berkas kosong akan dibuat.

Pada `fs.write()`metode tersebut menggantikan berkas dan konten tertentu jika memang ada. Jika tidak, maka akan terdapat berkas baru yang berisi konten sepesifik yang dimuat.  

## _Update_ Files
FS Module memiliki metode untuk memperbarui file:

* `fs.appendFile()`
* `fs.writeFile()`

Pada `fs.appendFile() diimplementasikan pada penambahan konten diakhir berkas. Sedangkan, pada `fs.writeFIle()` digunakan untuk mengganti berkas / konten.

## _Delete_ Berkas
Untuk menghapus berkas dengan menggunakan FS Module, digunakan `fs.unlink`.
